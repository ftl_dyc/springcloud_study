package com.fankexinxi.eureka_client.Controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "test")
@RestController
public class TestController {

    @Value("${server.port}")
    private String port;

    @GetMapping(value = "testEureka")
    public String testEureka(@RequestParam String name) {
        return "hi: " + name + ", i am from port " + port;
    }

}
