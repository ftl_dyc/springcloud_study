package com.fankexinxi.controller;


import com.fankexinxi.service.TestService;
import com.fankexinxi.utils.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "test")
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping(value = "insertTest")
    public Msg insertTest(@RequestParam String name,
                          @RequestParam Integer age) {
        Msg msg = testService.insertTest(name, age);
        if (msg.getCode() == 200) {
            return Msg.success();
        } else {
            return Msg.fail();
        }
    }
}
