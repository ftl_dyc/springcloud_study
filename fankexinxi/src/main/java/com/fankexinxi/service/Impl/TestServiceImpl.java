package com.fankexinxi.service.Impl;

import com.fankexinxi.mapper.TestMapper;
import com.fankexinxi.pojo.Test;
import com.fankexinxi.service.TestService;
import com.fankexinxi.utils.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public Msg insertTest(String name, Integer age) {
        Test test = Test.builder().name(name).age(age).build();
        int i = testMapper.insertSelective(test);
        return Msg.success();
    }
}
