package com.fankexinxi.service;

import com.fankexinxi.utils.Msg;

public interface TestService {
    Msg insertTest(String name, Integer age);
}
