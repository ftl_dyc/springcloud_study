package com.fankexinxi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.fankexinxi.mapper")
public class FankexinxiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FankexinxiApplication.class, args);
    }

}
