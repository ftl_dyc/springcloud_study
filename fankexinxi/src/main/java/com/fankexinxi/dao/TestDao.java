package com.fankexinxi.dao;

import com.fankexinxi.pojo.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Component;

@Component
public interface TestDao extends JpaRepository<Test, Integer>, JpaSpecificationExecutor<Test> {

}
