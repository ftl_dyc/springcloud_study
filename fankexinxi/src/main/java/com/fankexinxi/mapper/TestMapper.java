package com.fankexinxi.mapper;


import com.fankexinxi.pojo.Test;
import com.fankexinxi.pojo.TestExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TestMapper {

    int insertSelective(Test record);

    List<Test> selectByExample(TestExample example);

    int updateByPrimaryKeySelective(Test record);

}