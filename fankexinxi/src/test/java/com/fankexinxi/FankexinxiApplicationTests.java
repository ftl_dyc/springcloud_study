package com.fankexinxi;

import com.fankexinxi.dao.TestDao;
import com.fankexinxi.mapper.TestMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FankexinxiApplicationTests {

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private TestDao testDao;

    @Test
    public void contextLoads() {
        com.fankexinxi.pojo.Test test = com.fankexinxi.pojo.Test.builder()
                .name("张三").age(20).build();
        com.fankexinxi.pojo.Test save = testDao.save(test);
        System.out.println(save.getId());
        System.out.println("...............");

    }

}
