package com.fankexinxi.feign.Controller;

import com.fankexinxi.feign.Service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignController {

    @Autowired
    private FeignService feignService;

    @GetMapping(value = "testFeign")
    public String testFeign(@RequestParam String name) {
        return feignService.testEureka(name);
    }
}
