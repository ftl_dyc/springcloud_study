package com.fankexinxi.feign.Service;

import com.fankexinxi.feign.Service.Impl.FeignServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "eureka-client",fallback = FeignServiceImpl.class)
public interface FeignService {

    @GetMapping(value = "test/testEureka")
    String testEureka(@RequestParam String name);
}
