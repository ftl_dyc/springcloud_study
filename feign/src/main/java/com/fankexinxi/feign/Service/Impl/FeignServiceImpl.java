package com.fankexinxi.feign.Service.Impl;

import com.fankexinxi.feign.Service.FeignService;
import org.springframework.stereotype.Component;

@Component
public class FeignServiceImpl implements FeignService {

    @Override
    public String testEureka(String name) {
        return "error: " + name;
    }

}
