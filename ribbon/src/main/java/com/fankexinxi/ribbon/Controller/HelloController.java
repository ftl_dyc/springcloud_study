package com.fankexinxi.ribbon.Controller;

import com.fankexinxi.ribbon.Service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "test")
    public String test(@RequestParam String name) {
        return helloService.HiService(name);
    }
}
