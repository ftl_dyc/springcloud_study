package com.fankexinxi.ribbon.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Qualifier;

@Service
public class HelloService {

    @Autowired
    private RestTemplate restTemplate;

    public String HiService(String name) {
        return restTemplate.getForObject("http://eureka-client/test/testEureka?name=" + name, String.class);

    }
}
